﻿using CoreBooru.Api.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreBooru.Api.Database.EF
{
    internal sealed class CoreBooruContext : DbContext
    {
        private readonly string _connectionString;

        public CoreBooruContext(string connectionString = @"Server=(localdb)\mssqllocaldb;Database=MyDatabase;Trusted_Connection=True;")
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<Thread> Threads { get; set; }
    }
}
