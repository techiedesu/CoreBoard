﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreBooru.Api.Database.Models
{
    internal class Post
    {
        public Guid Guid { get; set; }
        public bool IsOp { get; set; }
        public bool IsTitle { get; set; }
        public string Text { get; set; }
    }
}
