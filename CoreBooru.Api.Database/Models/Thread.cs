﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreBooru.Api.Database.Models
{
    internal class Thread
    {
        public Guid Guid { get; set; }
        public IEnumerable<Post> Posts { get; set; }
    }
}
