﻿using System;
using System.Runtime.Serialization;

namespace CoreBooru.Api.Structs
{
    /// <summary>
    /// Post contains user defined data. Element of thread.
    /// </summary>
    [Serializable]
    public struct Post : IEntityBase
    {
        public Guid PostId { get; set; }
        public Guid ThreadId { get; set; }
        public bool IsFirst { get; set; }
        public bool IsOp { get; set; }
        public string Text { get; set; }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
