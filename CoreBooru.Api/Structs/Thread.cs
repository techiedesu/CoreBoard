﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CoreBooru.Api.Structs
{
    /// <summary>
    /// Thread entity. Contains one or more posts. First post is header.
    /// </summary>
    [Serializable]
    public struct Thread : IEntityBase
    {
        public IEnumerable<Guid> PostsIds { get; set; }
        public string Text { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
