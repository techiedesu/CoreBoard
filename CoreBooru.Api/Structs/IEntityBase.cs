﻿using System.Runtime.Serialization;

namespace CoreBooru.Api.Structs
{
    public interface IEntityBase : ISerializable
    {
    }
}
