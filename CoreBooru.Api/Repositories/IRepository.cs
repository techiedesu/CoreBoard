﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoreBooru.Api.Structs;

namespace CoreBooru.Api.Repositories
{
    /// <summary>
    /// Entity Repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : IEntityBase
    {
        /// <summary>
        /// Get entity by Guid
        /// </summary>
        /// <param name="id">Entity Guid</param>
        /// <returns>Entity or null if empty</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Get collection of entities
        /// </summary>
        /// <returns>Collection of entities</returns>
        Task<IEnumerable<T>> ListAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate">Search </param>
        /// <returns></returns>
        Task<IEnumerable<T>> ListAsync(Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task DeleteAsync(T entity);
        Task EditAsync(T entity);
    }
}
