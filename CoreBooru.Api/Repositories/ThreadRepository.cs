﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoreBooru.Api.Database;
using CoreBooru.Api.Structs;

namespace CoreBooru.Api.Repositories
{
    /// <summary>
    /// TODO: Add Repository level exceptions
    /// </summary>
    public class ThreadRepository : IRepository<Thread>
    {
        protected IDbRepository<Thread> DbRepo;

        public ThreadRepository(IDbRepository<Thread> dbRepository)
        {
            DbRepo = dbRepository;
        }

        public async Task AddAsync(Thread entity)
        {
            await Task.Run(() => { DbRepo.AddAsync(entity).Wait(); });
        }

        public async Task DeleteAsync(Thread entity)
        {
            await Task.Run(() => { DbRepo.DeleteAsync(entity).Wait(); });
        }

        public async Task EditAsync(Thread entity)
        {
            await Task.Run(() => { DbRepo.EditAsync(entity).Wait(); });
        }

        public async Task<Thread> GetByIdAsync(Guid guid)
        {
            return await Task.Run(() => DbRepo.GetById(guid.ToString()));
        }

        public async Task<IEnumerable<Thread>> ListAsync()
        {
            return await Task.Run(() => DbRepo.List());
        }

        public Task<IEnumerable<Thread>> ListAsync(Expression<Func<Thread, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
